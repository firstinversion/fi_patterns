#include <fi/patterns/message/support/error.hpp>

#include <array>
#include <fmt/format.h>
#include <rapidjson/document.h>
#include <rapidjson/schema.h>
#include <rapidjson/stringbuffer.h>

namespace fi::patterns::message {

    constexpr auto parse_error_messages = std::array{"No error.",
                                                     "The document is empty.",
                                                     "The document root must not follow by other values.",
                                                     "Invalid _value.",
                                                     "Missing a _name for object member.",
                                                     "Missing a colon after a _name of object member.",
                                                     "Missing a comma or '}' after an object member.",
                                                     "Missing a comma or ']' after an array element.",
                                                     "Incorrect hex digit after \\u escape in string.",
                                                     "The surrogate pair in string is invalid.",
                                                     "Invalid escape character in string.",
                                                     "Missing a closing quotation mark in string.",
                                                     "Invalid encoding in string.",
                                                     "Number too big to be stored in double.",
                                                     "Miss fraction part in number.",
                                                     "Miss exponent in number.",
                                                     "Parsing was terminated.",
                                                     "Unspecific syntax error."};

    validation_error validation_error::build(const rapidjson::Document& doc) {
        return validation_error(fmt::format(
            "Failed to parse json at {}: {}", doc.GetErrorOffset(), parse_error_messages[doc.GetParseError()]));
    }

    validation_error validation_error::build(const rapidjson::Document&        doc,
                                             const rapidjson::SchemaValidator& validator) {
        auto isp = rapidjson::StringBuffer{};
        validator.GetInvalidSchemaPointer().StringifyUriFragment(isp);
        auto idp = rapidjson::StringBuffer{};
        validator.GetInvalidDocumentPointer().StringifyUriFragment(idp);

        return validation_error(fmt::format("Failed to validate json: {} violated '{}' rule specified at {}",
                                            idp.GetString(),
                                            validator.GetInvalidSchemaKeyword(),
                                            isp.GetString()));
    }

}  // namespace fi::patterns::message
