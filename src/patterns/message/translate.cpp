#include <fi/patterns/message/translate.hpp>

#include <fi/patterns/message/ast.hpp>
#include <fi/patterns/message/generator.hpp>
#include <fi/patterns/message/parser.hpp>
#include <fmt/format.h>
#include <fstream>

namespace fi::patterns::message::translate {

    void message_header(const fi::patterns::message::ast::options& options, const std::filesystem::path& schema_file,
                        const std::filesystem::path& header_file) {
        auto ast = fi::patterns::message::parser::parse(schema_file);
        std::filesystem::create_directories(header_file.parent_path());
        auto out = std::ofstream{header_file};
        if (!out.is_open())
            throw std::runtime_error(fmt::format("Could not open output file ({}).", header_file.string()));
        generator::message_header(out, options, ast);
    }

}  // namespace fi::patterns::message::translate
