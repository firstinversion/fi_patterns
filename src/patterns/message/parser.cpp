#include <fi/patterns/message/parser.hpp>

#include <fmt/format.h>
#include <fstream>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

namespace fi::patterns::message::parser {

    const rapidjson::Value& query(const rapidjson::Value& value, const std::string& name) {
        if (!value.IsObject()) throw std::invalid_argument("Query Failed: given non-object.");
        auto itr = value.FindMember(name.data());
        if (itr == value.MemberEnd()) throw std::invalid_argument("Query Failed: field not found in object.");
        return itr->value;
    }

    std::string query_string(const rapidjson::Value& value, const std::string& name) {
        const auto& val = query(value, name);
        if (!val.IsString()) throw std::invalid_argument("Query Failed: was not a string value.");
        return std::string{val.GetString(), val.GetStringLength()};
    }

    std::string query_string(const rapidjson::Value& value, const std::string& name, const std::string& default_) {
        if (!value.IsObject()) throw std::invalid_argument("Query Failed: given non-object.");
        auto itr = value.FindMember(name.data());
        if (itr == value.MemberEnd()) return default_;
        if (!itr->value.IsString()) throw std::invalid_argument("cpp_type was provided but was not a string.");
        return std::string{itr->value.GetString(), itr->value.GetStringLength()};
    }

    ast::type query_type(const rapidjson::Value& value) {
        auto itr = value.FindMember("type");
        if (itr == value.MemberEnd()) throw std::runtime_error("Query Failed: type not found in presumed object.");
        auto indicator = std::string{itr->value.GetString()};
        if (indicator == "object") return ast::type::object;
        if (indicator == "array") return ast::type::array;
        if (indicator == "string") return ast::type::string;
        if (indicator == "number") return ast::type::number;
        if (indicator == "integer") return ast::type::integer;
        if (indicator == "boolean") return ast::type::boolean;
        if (indicator == "null") return ast::type::null;
        throw std::runtime_error(fmt::format("Query Failed: {} is an unknown property type.", indicator));
    }

    ast::value inhale_value(const rapidjson::Value& value);

    std::unique_ptr<ast::object> inhale_object(const rapidjson::Value& value) {
        auto object = std::make_unique<ast::object>();

        auto props_itr = value.FindMember("properties");
        if (props_itr != value.MemberEnd()) {
            for (auto itr = props_itr->value.MemberBegin(); itr != props_itr->value.MemberEnd(); ++itr) {
                object->properties.push_back(std::pair{std::string{itr->name.GetString(), itr->name.GetStringLength()},
                                                       inhale_value(itr->value)});
            }
        }

        auto required_itr = value.FindMember("required");
        if (required_itr != value.MemberEnd()) {
            if (!required_itr->value.IsArray())
                throw std::runtime_error(fmt::format("Query Failed: required field in object was not array."));

            for (auto& val : required_itr->value.GetArray()) {
                if (!val.IsString())
                    throw std::runtime_error(fmt::format("Query Failed: required array element was not a string."));
                object->required.emplace_back(val.GetString(), val.GetStringLength());
            }
        }

        return object;
    }

    std::unique_ptr<ast::array> inhale_array(const rapidjson::Value& value) {
        auto items_itr = value.FindMember("items");
        if (items_itr == value.MemberEnd()) {
            return std::make_unique<ast::array>();
        } else {
            auto array   = std::make_unique<ast::array>();
            array->items = inhale_value(items_itr->value);
            return array;
        }
    }

    std::unique_ptr<ast::string> inhale_string(const rapidjson::Value& value) {
        return std::make_unique<ast::string>();
    }

    std::unique_ptr<ast::number> inhale_number(const rapidjson::Value& value) {
        auto cpp_type = query_string(value, "cpp_type", "double");
        return std::make_unique<ast::number>(std::move(cpp_type));
    }

    std::unique_ptr<ast::integer> inhale_integer(const rapidjson::Value& value) {
        auto cpp_type = query_string(value, "cpp_type", "int64_t");
        return std::make_unique<ast::integer>(std::move(cpp_type));
    }

    std::unique_ptr<ast::boolean> inhale_boolean(const rapidjson::Value& value) {
        return std::make_unique<ast::boolean>();
    }

    std::unique_ptr<ast::null> inhale_null(const rapidjson::Value& value) { return std::make_unique<ast::null>(); }

    ast::value inhale_value(const rapidjson::Value& value) {
        auto type = query_type(value);
        switch (type) {
        case ast::type::object: return {inhale_object(value)};
        case ast::type::array: return {inhale_array(value)};
        case ast::type::string: return {inhale_string(value)};
        case ast::type::number: return {inhale_number(value)};
        case ast::type::integer: return {inhale_integer(value)};
        case ast::type::boolean: return {inhale_boolean(value)};
        case ast::type::null: return {inhale_null(value)};
        }
    }

    ast::root_value walk_schema(const rapidjson::Document& document) {
        auto type = query_type(document);
        switch (type) {
        case ast::type::object: return {inhale_object(document)};
        case ast::type::array: return {inhale_array(document)};
        default:
            throw std::invalid_argument(
                fmt::format("Root value must be an 'object' or 'array', '{}' was found.", ast::string_of(type)));
        }
    }

    void validate_schema(const rapidjson::Document& document) {
        // for now assume ok, this is where you would validate the message document.
    }

    ast::message parse(const std::filesystem::path& schema_file) {
        if (!std::filesystem::exists(schema_file))
            throw std::invalid_argument(
                fmt::format("Provided message file does not exist ({}).", schema_file.string()));
        auto input_stream  = std::ifstream{schema_file};
        auto input_wrapper = rapidjson::IStreamWrapper{input_stream};
        auto document      = rapidjson::Document{};
        document.ParseStream(input_wrapper);
        if (document.HasParseError())
            throw std::runtime_error(
                fmt::format("JSON Parse Error ({}): \n{}", schema_file.string(), document.GetParseError()));

        return parse(document);
    }

    ast::message parse(const std::string& schema_string) {
        auto document = rapidjson::Document{};
        document.Parse(schema_string.data());
        return parse(document);
    }

    ast::message parse(const rapidjson::Document& schema_document) {
        const auto cpp_namespace = query_string(schema_document, "cpp_namespace");
        const auto cpp_class     = query_string(schema_document, "cpp_class");

        auto schema_buffer = rapidjson::StringBuffer{};
        auto schema_writer = rapidjson::Writer<rapidjson::StringBuffer>{schema_buffer};
        schema_document.Accept(schema_writer);

        return {cpp_namespace, cpp_class, {schema_buffer.GetString()}, walk_schema(schema_document)};
    }

}  // namespace fi::patterns::message::parser
