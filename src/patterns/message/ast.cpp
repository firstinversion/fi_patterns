#include <fi/patterns/message/ast.hpp>

namespace fi::patterns::message::ast {

    std::string string_of(type t) {
        switch (t) {
        case type::object: return "object";
        case type::array: return "array";
        case type::string: return "string";
        case type::number: return "number";
        case type::integer: return "integer";
        case type::boolean: return "boolean";
        case type::null: return "null";
        }
    }

}  // namespace fi::patterns::message::ast
