#include <fi/patterns/message/generator.hpp>

namespace fi::patterns::message::generator {

    std::string items_type_name(const ast::value& val) {
        if (std::holds_alternative<std::unique_ptr<ast::object>>(val)) {
            return "object";
        } else if (std::holds_alternative<std::unique_ptr<ast::array>>(val)) {
            return fmt::format("fi::patterns::message::support::array<{}>",
                               items_type_name(std::get<std::unique_ptr<ast::array>>(val)->items));
        } else if (std::holds_alternative<std::unique_ptr<ast::string>>(val)) {
            return "std::string_view";
        } else if (std::holds_alternative<std::unique_ptr<ast::number>>(val)) {
            return std::get<std::unique_ptr<ast::number>>(val)->cpp_type;
        } else if (std::holds_alternative<std::unique_ptr<ast::integer>>(val)) {
            return std::get<std::unique_ptr<ast::integer>>(val)->cpp_type;
        } else if (std::holds_alternative<std::unique_ptr<ast::boolean>>(val)) {
            return "bool";
        } else if (std::holds_alternative<std::unique_ptr<ast::null>>(val)) {
            return "void";
        } else {
            return "void";
        }
    }

}  // namespace fi::patterns::message::generator
