#include <catch2/catch.hpp>

#include <fi/patterns/message/generator.hpp>
#include <sstream>
#include <string>

auto basic_options() { return fi::patterns::message::ast::options{}; }

auto basic_ast() {
    using namespace fi::patterns::message::ast;
    return message{"test", "object", "{}", {std::make_unique<object>()}};
}

TEST_CASE("unit: message.generator: call generate message header from std::stringstream", "[unit]") {
    std::stringstream ss;
    fi::patterns::message::generator::message_header(ss, basic_options(), basic_ast());
}
