#include <catch2/catch.hpp>

#include <fi/patterns/string.hpp>
#include <rapidjson/document.h>
#include <variant>

TEST_CASE("unit: translate(cartesian)", "[unit]") {
    fi::patterns::message::translate::message_header(
        {}, "./test/resources/message/cartesian.json", "./tmp/unit/cartesian.hpp");
}

TEST_CASE("unit: translate(string)", "[unit]") {
    fi::patterns::message::translate::message_header(
        {}, "./test/resources/message/string.json", "./tmp/unit/string.hpp");
}

TEST_CASE("unit: inhale_adt(cartesian)", "[unit]") {
    auto ast = fi::patterns::message::parser::parse(std::filesystem::path{"./test/resources/message/cartesian.json"});

    REQUIRE(std::holds_alternative<std::unique_ptr<fi::patterns::message::ast::object>>(ast.root));
    auto& properties = std::get<std::unique_ptr<fi::patterns::message::ast::object>>(ast.root)->properties;
    REQUIRE(properties.size() == 3);
    REQUIRE(properties[0].first == "x");
    REQUIRE(std::holds_alternative<std::unique_ptr<fi::patterns::message::ast::number>>(properties[0].second));
    REQUIRE(properties[1].first == "y");
    REQUIRE(std::holds_alternative<std::unique_ptr<fi::patterns::message::ast::number>>(properties[1].second));
    REQUIRE(properties[2].first == "z");
    REQUIRE(std::holds_alternative<std::unique_ptr<fi::patterns::message::ast::number>>(properties[2].second));
}
