#include <catch2/catch.hpp>

#include <string>
#include <test/resources/embed/hello.txt.hpp>

TEST_CASE("Embedded hello message", "[integration,embed]") {
    auto data = std::string{embed::resource::hello_txt, embed::resource::hello_txt_len};
    REQUIRE(data == "Hello\n\n");
}
