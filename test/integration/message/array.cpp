#include <catch2/catch.hpp>

#include <rapidjson/document.h>
#include <test/resources/message/array_object_toplevel.json.hpp>
#include <test/resources/message/array_toplevel.json.hpp>
#include <test/resources/message/arrays.json.hpp>
#include <type_traits>

TEST_CASE("message: parse array of object", "[integration,message]") {
    static const char data[] = R"({"array_of_object": [{"x": 1.0, "y": 2.0}, {"x": 3.0, "y": 4.0}]})";
    const auto        msg    = test::message::arrays_property{data};

    REQUIRE(msg.array_of_object().size() == 2);
    REQUIRE(msg.array_of_object()[0].x() == 1.0);
    REQUIRE(msg.array_of_object()[0].y() == 2.0);
    REQUIRE(msg.array_of_object()[1].x() == 3.0);
    REQUIRE(msg.array_of_object()[1].y() == 4.0);
}

TEST_CASE("message: parse array of array of string", "[integration,message]") {
    static const char data[] = R"({"array_of_array_of_string":[["hello"], ["world"]]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_array_of_string()[0]),
                                 fi::patterns::message::support::array<std::string_view>>);
    REQUIRE(msg.array_of_array_of_string().size() == 2);

    static_assert(std::is_same_v<decltype(msg.array_of_array_of_string()[0][0]), std::string_view>);
    REQUIRE(msg.array_of_array_of_string()[0].size() == 1);
    REQUIRE(msg.array_of_array_of_string()[0][0] == "hello");
    REQUIRE(msg.array_of_array_of_string()[1][0] == "world");
}

TEST_CASE("message: parse array of string", "[integration,message]") {
    static const char data[] = R"({"array_of_string": ["hello", "world"]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_string()[0]), std::string_view>);
    REQUIRE(msg.array_of_string().size() == 2);
    REQUIRE(msg.array_of_string()[0] == "hello");
    REQUIRE(msg.array_of_string()[1] == "world");
}

TEST_CASE("message: parse array of number", "[integration,message]") {
    static const char data[] = R"({"array_of_number": [1.0, 2.0, 3.0]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_number()[0]), double>);
    REQUIRE(msg.array_of_number().size() == 3);
    REQUIRE(msg.array_of_number()[0] == 1.0);
    REQUIRE(msg.array_of_number()[1] == 2.0);
    REQUIRE(msg.array_of_number()[2] == 3.0);
}

TEST_CASE("message: parse array of integer", "[integration,message]") {
    static const char data[] = R"({"array_of_integer": [1, 2, 3, 4, 5]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_integer()[0]), int64_t>);
    REQUIRE(msg.array_of_integer().size() == 5);
    REQUIRE(msg.array_of_integer()[0] == 1);
    REQUIRE(msg.array_of_integer()[1] == 2);
    REQUIRE(msg.array_of_integer()[4] == 5);
}

TEST_CASE("message: parse array of double", "[integration,message]") {
    static const char data[] = R"({"array_of_double": [1.0, 2.0, 3.0]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_double()[0]), double>);
    REQUIRE(msg.array_of_double().size() == 3);
    REQUIRE(msg.array_of_double()[0] == 1.0);
    REQUIRE(msg.array_of_double()[1] == 2.0);
    REQUIRE(msg.array_of_double()[2] == 3.0);
}

TEST_CASE("message: parse array of float", "[integration,message]") {
    static const char data[] = R"({"array_of_float": [1.0, 2.0, 3.0]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_float()[0]), float>);
    REQUIRE(msg.array_of_float().size() == 3);
    REQUIRE(msg.array_of_float()[0] == 1.0);
    REQUIRE(msg.array_of_float()[1] == 2.0);
    REQUIRE(msg.array_of_float()[2] == 3.0);
}

TEST_CASE("message: parse array of int64", "[integration,message]") {
    static const char data[] = R"({"array_of_int64": [1, 2, 3, 4, 5]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_int64()[0]), int64_t>);
    REQUIRE(msg.array_of_int64().size() == 5);
    REQUIRE(msg.array_of_int64()[0] == 1);
    REQUIRE(msg.array_of_int64()[1] == 2);
    REQUIRE(msg.array_of_int64()[4] == 5);
}

TEST_CASE("message: parse array of uint64", "[integration,message]") {
    static const char data[] = R"({"array_of_uint64": [1, 2, 3, 4, 5]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_uint64()[0]), uint64_t>);
    REQUIRE(msg.array_of_uint64().size() == 5);
    REQUIRE(msg.array_of_uint64()[0] == 1);
    REQUIRE(msg.array_of_uint64()[1] == 2);
    REQUIRE(msg.array_of_uint64()[4] == 5);
}

TEST_CASE("message: parse array of int32", "[integration,message]") {
    static const char data[] = R"({"array_of_int32": [1, 2, 3, 4, 5]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_int32()[0]), int32_t>);
    REQUIRE(msg.array_of_int32().size() == 5);
    REQUIRE(msg.array_of_int32()[0] == 1);
    REQUIRE(msg.array_of_int32()[1] == 2);
    REQUIRE(msg.array_of_int32()[4] == 5);
}

TEST_CASE("message: parse array of uint32", "[integration,message]") {
    static const char data[] = R"({"array_of_uint32": [1, 2, 3, 4, 5]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_uint32()[0]), uint32_t>);
    REQUIRE(msg.array_of_uint32().size() == 5);
    REQUIRE(msg.array_of_uint32()[0] == 1);
    REQUIRE(msg.array_of_uint32()[1] == 2);
    REQUIRE(msg.array_of_uint32()[4] == 5);
}

TEST_CASE("message: parse array of boolean", "[integration,message]") {
    static const char data[] = R"({"array_of_bool": [true, false, true]})";
    const auto        msg    = test::message::arrays_property{data};

    static_assert(std::is_same_v<decltype(msg.array_of_bool()[0]), bool>);
    REQUIRE(msg.array_of_bool().size() == 3);
    REQUIRE(msg.array_of_bool()[0]);
    REQUIRE(!msg.array_of_bool()[1]);
    REQUIRE(msg.array_of_bool()[2]);
}

TEST_CASE("message: parse array with missing data", "[integration,message]") {
    static const char data[] = R"({})";
    const auto        msg    = test::message::arrays_property{data};

    REQUIRE(msg.array_of_bool().empty());
}

TEST_CASE("message: parse toplevel array.", "[integration,message]") {
    static const char data[] = R"([1, 2, 3, 4])";
    const auto        msg    = test::message::array_toplevel{data};

    REQUIRE(msg.size() == 4);
    REQUIRE(msg[0] == 1);
    REQUIRE(msg[1] == 2);
    REQUIRE(msg[2] == 3);
    REQUIRE(msg[3] == 4);
}

TEST_CASE("message: toplevel array of object", "[integration,message]") {
    static const char data[] = R"([{"x": 1.0, "y": 2.0}, {"x": 3.0, "y": 4.0}])";
    const auto        msg    = test::message::array_object_toplevel{data};

    REQUIRE(msg.size() == 2);
    REQUIRE(msg[0].x() == 1.0);
    REQUIRE(msg[0].y() == 2.0);
    REQUIRE(msg[1].x() == 3.0);
    REQUIRE(msg[1].y() == 4.0);
}
