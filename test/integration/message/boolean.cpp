#include <catch2/catch.hpp>

#include <rapidjson/document.h>
#include <test/resources/message/boolean.json.hpp>

TEST_CASE("message: boolean default", "[integration,message]") {
    static const char data[] = R"({"exists": true})";
    const auto        msg    = test::message::boolean_property{data};

    REQUIRE(msg.exists());
}
