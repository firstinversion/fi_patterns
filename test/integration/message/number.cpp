#include <catch2/catch.hpp>

#include <rapidjson/document.h>
#include <test/resources/message/number.json.hpp>

TEST_CASE("message: number default", "[integration,message]") {
    static const char data[] = R"({"double_": 30.0})";
    const auto        msg    = test::message::number_property{data};

    REQUIRE(msg.double_() == 30.0);
}

TEST_CASE("message: number multiple", "[integration,message]") {
    try {
        static const char fail_data[] = R"({"double_multiple": 29.0})";
        const auto        fail_msg    = test::message::number_property{fail_data};
        assert(false);
    } catch (const std::exception& e) {
    }

    static const char succeed_data[] = R"({"double_multiple": 30.0})";
    const auto        succeed_msg    = test::message::number_property{succeed_data};
    REQUIRE(succeed_msg.double_multiple() == 30.0);
}

TEST_CASE("message: number range", "[integration,message]") {
    try {
        static const char fail_data[] = R"({"double_range": 4.0})";
        const auto        fail_msg    = test::message::number_property{fail_data};
        assert(false);
    } catch (const std::exception& e) {
    }

    try {
        static const char fail_data[] = R"({"double_range": 11.0})";
        const auto        fail_msg    = test::message::number_property{fail_data};
        assert(false);
    } catch (const std::exception& e) {
    }

    static const char succeed_data[] = R"({"double_range": 6.0})";
    const auto        succeed_msg    = test::message::number_property{succeed_data};
    REQUIRE(succeed_msg.double_range() == 6);
}

TEST_CASE("message: number float", "[integration,message]") {
    static const char succeed_data[] = R"({"double_float": 57.3})";
    const auto        succeed_msg    = test::message::number_property{succeed_data};
    REQUIRE(succeed_msg.double_float() == 57.3f);
}

TEST_CASE("message: integer default", "[integration,message]") {
    static const char data[] = R"({"integer_": 30})";
    const auto        msg    = test::message::number_property{data};

    REQUIRE(msg.integer_() == int64_t{30});
}

TEST_CASE("message: integer multiple", "[integration,message]") {
    try {
        static const char fail_data[] = R"({"integer_multiple": 29})";
        const auto        fail_msg    = test::message::number_property{fail_data};
        assert(false);
    } catch (const std::exception& e) {
    }

    static const char succeed_data[] = R"({"integer_multiple": 30})";
    const auto        succeed_msg    = test::message::number_property{succeed_data};
    REQUIRE(succeed_msg.integer_multiple() == int64_t{30});
}

TEST_CASE("message: integer range", "[integration,message]") {
    try {
        static const char fail_data[] = R"({"integer_range": 4})";
        const auto        fail_msg    = test::message::number_property{fail_data};
        assert(false);
    } catch (const std::exception& e) {
    }

    try {
        static const char fail_data[] = R"({"integer_range": 11})";
        const auto        fail_msg    = test::message::number_property{fail_data};
        assert(false);
    } catch (const std::exception& e) {
    }

    static const char succeed_data[] = R"({"integer_range": 6})";
    const auto        succeed_msg    = test::message::number_property{succeed_data};
    REQUIRE(succeed_msg.integer_range() == int64_t{6});
}

TEST_CASE("message: integer uint32", "[integration,message]") {
    static const char succeed_data[] = R"({"integer_uint32": 843})";
    const auto        succeed_msg    = test::message::number_property{succeed_data};
    REQUIRE(succeed_msg.integer_uint32() == uint32_t{843});
}

TEST_CASE("message: integer int32", "[integration,message]") {
    static const char succeed_data[] = R"({"integer_int32": 8275})";
    const auto        succeed_msg    = test::message::number_property{succeed_data};
    REQUIRE(succeed_msg.integer_int32() == int32_t{8275});
}

TEST_CASE("message: integer uint64", "[integration,message]") {
    static const char succeed_data[] = R"({"integer_uint64": 8275})";
    const auto        succeed_msg    = test::message::number_property{succeed_data};
    REQUIRE(succeed_msg.integer_uint64() == uint64_t{8275});
}

TEST_CASE("message: integer int64", "[integration,message]") {
    static const char succeed_data[] = R"({"integer_int64": 843})";
    const auto        succeed_msg    = test::message::number_property{succeed_data};
    REQUIRE(succeed_msg.integer_int64() == int64_t{843});
}
