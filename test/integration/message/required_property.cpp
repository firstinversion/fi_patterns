#include <catch2/catch.hpp>

#include <rapidjson/document.h>
#include <test/resources/message/required_property.json.hpp>

static const char data[] = R"({
    "string": "hello",
    "double_": 83.0,
    "float_": 33.0,
    "integer_32": 24,
    "integer_u32": 35,
    "integer_64": 752,
    "integer_u64": 725
})";

TEST_CASE("message: required string", "[integration,message]") {
    const auto msg = test::message::required_property{data};
    static_assert(std::is_same_v<decltype(msg.string()), std::string_view>);
    REQUIRE(msg.string() == "hello");
}

TEST_CASE("message: required double", "[integration,message]") {
    const auto msg = test::message::required_property{data};
    static_assert(std::is_same_v<decltype(msg.double_()), double>);
    REQUIRE(msg.double_() == 83.0);
}

TEST_CASE("message: required float", "[integration,message]") {
    const auto msg = test::message::required_property{data};
    static_assert(std::is_same_v<decltype(msg.float_()), float>);
    REQUIRE(msg.float_() == 33.0);
}

TEST_CASE("message: required int32", "[integration,message]") {
    const auto msg = test::message::required_property{data};
    static_assert(std::is_same_v<decltype(msg.integer_32()), int32_t>);
    REQUIRE(msg.integer_32() == 24);
}

TEST_CASE("message: required uint32", "[integration,message]") {
    const auto msg = test::message::required_property{data};
    static_assert(std::is_same_v<decltype(msg.integer_u32()), uint32_t>);
    REQUIRE(msg.integer_u32() == 35);
}

TEST_CASE("message: required int64", "[integration,message]") {
    const auto msg = test::message::required_property{data};
    static_assert(std::is_same_v<decltype(msg.integer_64()), int64_t>);
    REQUIRE(msg.integer_64() == 752);
}

TEST_CASE("message: required uint64", "[integration,message]") {
    const auto msg = test::message::required_property{data};
    static_assert(std::is_same_v<decltype(msg.integer_u64()), uint64_t>);
    REQUIRE(msg.integer_u64() == 725);
}
