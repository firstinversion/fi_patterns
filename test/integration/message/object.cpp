#include <catch2/catch.hpp>

#include <rapidjson/document.h>
#include <test/resources/message/object.json.hpp>
#include <test/resources/message/object_required.json.hpp>
#include <test/resources/message/object_required_array.json.hpp>
#include <type_traits>

TEST_CASE("message: parse object field", "[integration,message]") {
    static const char data[] = R"({"point":{"x":1.0, "y":2.0, "z":3.0}})";
    const auto        msg    = test::message::object_property{data};

    static_assert(std::is_same_v<decltype(msg.point()), std::optional<test::message::object_property::point_object>>);
    REQUIRE(msg.point());
    REQUIRE(msg.point().value().x() == 1.0);
    REQUIRE(msg.point().value().y() == 2.0);
    REQUIRE(msg.point().value().z() == 3.0);
}

TEST_CASE("message: parse object required field", "[integration,message]") {
    static const char data[] = R"({"point":{"x":1.0, "y":2.0, "z":3.0}})";
    const auto        msg    = test::message::object_required_property{data};

    static_assert(std::is_same_v<decltype(msg.point()), test::message::object_required_property::point_object>);
    REQUIRE(msg.point().x() == 1.0);
    REQUIRE(msg.point().y() == 2.0);
    REQUIRE(msg.point().z() == 3.0);
}

TEST_CASE("message: change value", "[integration,message]") {
    static const char data[] = R"({"point":{"x":1.0, "y":2.0, "z":3.0}})";
    const auto        msg    = test::message::object_required_property{data};

    REQUIRE(msg.point().x() == 1.0);
    msg.point().x(22.0);
    REQUIRE(msg.point().x() == 22.0);
}

TEST_CASE("message: required array field", "[integration,message]") {
    static const char data[] = R"({"points": [{"x": 1, "y": 2}, {"x": 3, "y": 4}]})";
    const auto msg = test::message::object_required_array{data};

    REQUIRE(msg.points().size() == 2);
    REQUIRE(msg.points()[0].x() == 1);
    REQUIRE(msg.points()[1].x() == 3);

    const auto gen = test::message::object_required_array{};
    auto p = gen.points().insert();
    p.x(1);
    p.y(2);
}
