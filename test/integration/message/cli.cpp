#include <catch2/catch.hpp>

#include <boost/process.hpp>
#include <filesystem>

TEST_CASE("integration cli: cartesian", "[integration]") {
    std::filesystem::remove("./tmp/classes/cartesian.json");
    int result = boost::process::system(
        "./generate_message -i ./test/resources/message/cartesian.json -o "
        "./tmp/integration/cartesian.hpp");
    REQUIRE(result == 0);
}
