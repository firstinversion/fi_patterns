#include <catch2/catch.hpp>

#include <rapidjson/document.h>
#include <test/resources/message/string.json.hpp>
#include <type_traits>

TEST_CASE("message: string default", "[integration,message]") {
    static const char data[] = R"({"default_": "hello"})";
    const auto        msg    = test::message::string_property{data};

    static_assert(std::is_same_v<decltype(msg.default_()), std::optional<std::string_view>>);
    REQUIRE(msg.default_() == "hello");
}

TEST_CASE("message: string min length", "[integration,message]") {
    try {
        static const char fail_data[] = R"({"min_length": "d"})";
        const auto        fail_msg    = test::message::string_property{fail_data};
        assert(false);
    } catch (const std::exception& e) {
    }

    static const char succeed_data[] = R"({"min_length": "dd"})";
    const auto        succeed_msg    = test::message::string_property{succeed_data};
    static_assert(std::is_same_v<decltype(succeed_msg.min_length()), std::optional<std::string_view>>);
    REQUIRE(succeed_msg.min_length() == "dd");
}

TEST_CASE("message: string max length", "[integration,message]") {
    try {
        static const char fail_data[] = R"({"max_length": "dddd"})";
        const auto        fail_msg    = test::message::string_property{fail_data};
        assert(false);
    } catch (const std::exception& e) {
    }

    static const char succeed_data[] = R"({"max_length": "ddd"})";
    const auto        succeed_msg    = test::message::string_property{succeed_data};
    static_assert(std::is_same_v<decltype(succeed_msg.max_length()), std::optional<std::string_view>>);
    REQUIRE(succeed_msg.max_length() == "ddd");
}

TEST_CASE("message: string regular expression", "[integration,message]") {
    try {
        static const char fail_data[] = R"({"regex": "hello"})";
        const auto        fail_msg    = test::message::string_property{fail_data};
        assert(false);
    } catch (const std::exception& e) {
    }

    static const char succeed_data[] = R"({"regex": "555-1212"})";
    const auto        succeed_msg    = test::message::string_property{succeed_data};
    static_assert(std::is_same_v<decltype(succeed_msg.regex()), std::optional<std::string_view>>);
    REQUIRE(succeed_msg.regex() == "555-1212");
}
