#include <catch2/catch.hpp>

#include <rapidjson/document.h>
#include <test/resources/message/cartesian.json.hpp>

TEST_CASE("cmake command: cartesian render", "[integration]") {
    auto test = test::message::cartesian{};
    test.x(35);
    test.y(83);
    test.z(85);

    const auto& doc = test.to_document();
    REQUIRE(doc.FindMember("x") != doc.MemberEnd());
    REQUIRE(doc.FindMember("y") != doc.MemberEnd());
    REQUIRE(doc.FindMember("z") != doc.MemberEnd());

    REQUIRE(doc["x"].GetDouble() == 35);
    REQUIRE(doc["y"].GetDouble() == 83);
    REQUIRE(doc["z"].GetDouble() == 85);
}

TEST_CASE("cmake command: cartesian parse", "[integration]") {
    static const char data[] = R"({"x": 35.0, "y": 83.0, "z": 85.0})";
    auto              test   = test::message::cartesian{data};

    REQUIRE(test.x() == 35.0);
    REQUIRE(test.y() == 83.0);
    REQUIRE(test.z() == 85.0);
}
