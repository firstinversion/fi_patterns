#pragma once

#include <fi/patterns/export.hpp>
#include <fi/patterns/message/ast.hpp>
#include <filesystem>

namespace fi::patterns::message::translate {

    FI_PATTERNS_EXPORT void message_header(const fi::patterns::message::ast::options& options,
                                           const std::filesystem::path&               schema_file,
                                           const std::filesystem::path&               header_file);

}
