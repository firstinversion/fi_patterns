#pragma once

#include <fi/patterns/export.hpp>
#include <fi/patterns/message/ast.hpp>
#include <filesystem>
#include <rapidjson/fwd.h>

namespace fi::patterns::message::parser {

    FI_PATTERNS_EXPORT ast::message parse(const std::filesystem::path& schema_file);

    FI_PATTERNS_EXPORT ast::message parse(const std::string& schema_string);

    FI_PATTERNS_EXPORT ast::message parse(const rapidjson::Document& schema_document);

}  // namespace fi::patterns::message::parser
