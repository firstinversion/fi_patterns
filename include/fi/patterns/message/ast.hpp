#pragma once

#include <fi/patterns/export.hpp>
#include <memory>
#include <string>
#include <variant>
#include <vector>

namespace fi::patterns::message::ast {

    enum class type { object, array, string, number, integer, boolean, null };

    FI_PATTERNS_EXPORT std::string string_of(type t);

    struct object;
    struct array;
    struct string;
    struct number;
    struct integer;
    struct boolean;
    struct null;

    // clang-format off
    using value = std::variant<
    std::unique_ptr<object>,
    std::unique_ptr<array>,
    std::unique_ptr<string>,
    std::unique_ptr<number>,
    std::unique_ptr<integer>,
    std::unique_ptr<boolean>,
    std::unique_ptr<null>>;

    using root_value = std::variant<
    std::unique_ptr<object>,
    std::unique_ptr<array>>;
    // clang-format on

    struct object {
        std::vector<std::pair<std::string, value>> properties;
        std::vector<std::string>                   required;
    };

    struct array {
        value items;
    };

    struct string {};

    struct number {
        std::string cpp_type;

        explicit number(std::string cpp_type)
            : cpp_type(std::move(cpp_type)) {}
    };

    struct integer {
        std::string cpp_type;

        explicit integer(std::string cpp_type)
            : cpp_type(std::move(cpp_type)) {}
    };

    struct boolean {};

    struct null {};

    struct message {
        std::string cpp_namespace;
        std::string cpp_class;
        std::string schema;
        root_value  root;
    };

    struct options {};

}  // namespace fi::patterns::message::ast
