#pragma once

#include <fi/patterns/export.hpp>
#include <fi/patterns/generator/escape.hpp>
#include <fi/patterns/message/ast.hpp>
#include <filesystem>
#include <fmt/format.h>

namespace fi::patterns::message::generator {

    template <typename OutStream>
    void message_object_constructors(OutStream& out, const ast::options& opt, const ast::message& message) {
        // clang-format off
        out << "    " << message.cpp_class << "() : fi::patterns::message::support::message(rapidjson::kObjectType), fi::patterns::message::support::object(_document, _document.GetAllocator()) {}\n";

        out << "    explicit " << message.cpp_class << "(const char* input) : fi::patterns::message::support::message(), fi::patterns::message::support::object(_document, _document.GetAllocator()) {\n";
        out << "        _document.Parse(input);\n";
        out << "        validate_throw();\n";
        out << "    }\n";

        out << "    template <typename InputStream>\n";
        out << "    explicit " << message.cpp_class << "(InputStream& input) : fi::patterns::message::support::message(), fi::patterns::message::support::object(_document, _document.GetAllocator()) {\n";
        out << "        _document.ParseStream(input);\n";
        out << "        validate_throw();\n";
        out << "    }\n";
        // clang-format on
    }

    template <typename OutStream>
    void message_array_constructors(OutStream& out, const ast::options& opt, const ast::message& message,
                                    const std::string& subtype) {
        // clang-format off
        out << "    " << message.cpp_class << "() : fi::patterns::message::support::message(rapidjson::kArrayType), fi::patterns::message::support::array<" << subtype << ">(_document, _document.GetAllocator()) {}\n";

        out << "    explicit " << message.cpp_class << "(const char* input) : fi::patterns::message::support::message(), fi::patterns::message::support::array<" << subtype << ">(_document, _document.GetAllocator()) {\n";
        out << "        _document.Parse(input);\n";
        out << "        validate_throw();\n";
        out << "    }\n";

        out << "    template <typename InputStream>\n";
        out << "    explicit " << message.cpp_class << "(InputStream& input) : fi::patterns::message::support::message(), fi::patterns::message::support::array<" << subtype << ">(_document, _document.GetAllocator()) {\n";
        out << "        _document.ParseStream(input);\n";
        out << "        validate_throw();\n";
        out << "    }\n";
        // clang-format on
    }

    template <typename OutStream>
    void object_constructors(OutStream& out, const ast::options& opt, const ast::object& object,
                             const std::string& property_name) {
        out << "    " << property_name << "(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator)\n";
        out << "        : fi::patterns::message::support::object(value, allocator) {}\n";
    }

    template <typename OutStream>
    void validation_functions(OutStream& out, const ast::options& opt, const ast::message& message) {
        // clang-format off
        out << "    std::optional<fi::patterns::message::validation_error> validate() noexcept {\n";
        out << "        return validate_impl(message_schema());\n";
        out << "    }\n";

        out << "    void validate_throw() {\n";
        out << "        validate_throw_impl(message_schema());\n";
        out << "    }\n";
        // clang-format on
    }

    template <typename OutStream>
    void getters(OutStream& out, const std::string& name, const std::string& type_name, bool required) {
        out << "    [[nodiscard]] auto " << name << "() const {\n";
        out << "        return fi::patterns::message::support::accessor<" << type_name << ", "
            << (required ? "true" : "false") << ">().get_value(_value, _allocator, \"" << name << "\");\n";
        out << "    }\n";

        out << "    [[nodiscard]] auto get_" << name << "() const {\n";
        out << "        return fi::patterns::message::support::accessor<" << type_name << ", "
            << (required ? "true" : "false") << ">().get_value(_value, _allocator, \"" << name << "\");\n";
        out << "    }\n";
    }

    template <typename OutStream>
    void setters(OutStream& out, const std::string& name, const std::string& type_name, bool required) {
        out << "    void " << name << "(" << type_name << " val) {\n";
        out << "        fi::patterns::message::support::accessor<" << type_name << ", " << (required ? "true" : "false")
            << ">().set_value(_value, _allocator, \"" << name << "\", val);\n";
        out << "    }\n";

        out << "    void set_" << name << "(" << type_name << " val) {\n";
        out << "        fi::patterns::message::support::accessor<" << type_name << ", " << (required ? "true" : "false")
            << ">().set_value(_value, _allocator, \"" << name << "\", val);\n";
        out << "    }\n";
    }

    template <typename OutStream>
    void object_class(OutStream& out, const ast::options& opt, const ast::object& object,
                      const std::string& class_name);

    template <typename OutStream>
    void object_property(OutStream& out, const ast::options& opt, const ast::object& parent, const std::string& name,
                         const ast::object& val) {
        bool required = std::count(parent.required.begin(), parent.required.end(), name);
        object_class(out, opt, val, fmt::format("{}_object", name));
        getters(out, name, fmt::format("{}_object", name), required);
    }

    FI_PATTERNS_EXPORT std::string items_type_name(const ast::value& val);

    template <typename OutStream>
    void object_property(OutStream& out, const ast::options& opt, const ast::object& parent, const std::string& name,
                         const ast::array& val) {
        bool required      = std::count(parent.required.begin(), parent.required.end(), name);
        auto interior_type = items_type_name(val.items);

        if (interior_type == "object") {
            object_class(out, opt, *std::get<std::unique_ptr<ast::object>>(val.items), fmt::format("{}_object", name));
            getters(out, name, fmt::format("fi::patterns::message::support::array<{}_object>", name), required);
        } else {
            getters(out, name, fmt::format("fi::patterns::message::support::array<{}>", interior_type), required);
        }
    }

    template <typename OutStream>
    void object_property(OutStream& out, const ast::options& opt, const ast::object& parent, const std::string& name,
                         const ast::string& val) {
        bool required = std::count(parent.required.begin(), parent.required.end(), name);
        getters(out, name, "std::string_view", required);
        setters(out, name, "std::string_view", required);
    }

    template <typename OutStream>
    void object_property(OutStream& out, const ast::options& opt, const ast::object& parent, const std::string& name,
                         const ast::number& val) {
        bool required = std::count(parent.required.begin(), parent.required.end(), name);
        getters(out, name, val.cpp_type, required);
        setters(out, name, val.cpp_type, required);
    }

    template <typename OutStream>
    void object_property(OutStream& out, const ast::options& opt, const ast::object& parent, const std::string& name,
                         const ast::integer& val) {
        bool required = std::count(parent.required.begin(), parent.required.end(), name);
        getters(out, name, val.cpp_type, required);
        setters(out, name, val.cpp_type, required);
    }

    template <typename OutStream>
    void object_property(OutStream& out, const ast::options& opt, const ast::object& parent, const std::string& name,
                         const ast::boolean& val) {
        bool required = std::count(parent.required.begin(), parent.required.end(), name);
        getters(out, name, "bool", required);
        setters(out, name, "bool", required);
    }

    template <typename OutStream>
    void object_property(OutStream& out, const ast::options& opt, const ast::object& parent, const std::string& name,
                         const ast::null& val) {}

    template <typename OutStream>
    void object_property(OutStream& out, const ast::options& opt, const ast::object& parent, const std::string& name,
                         const ast::value& val) {
        if (std::holds_alternative<std::unique_ptr<ast::object>>(val)) {
            object_property(out, opt, parent, name, *std::get<std::unique_ptr<ast::object>>(val));
        } else if (std::holds_alternative<std::unique_ptr<ast::array>>(val)) {
            object_property(out, opt, parent, name, *std::get<std::unique_ptr<ast::array>>(val));
        } else if (std::holds_alternative<std::unique_ptr<ast::string>>(val)) {
            object_property(out, opt, parent, name, *std::get<std::unique_ptr<ast::string>>(val));
        } else if (std::holds_alternative<std::unique_ptr<ast::number>>(val)) {
            object_property(out, opt, parent, name, *std::get<std::unique_ptr<ast::number>>(val));
        } else if (std::holds_alternative<std::unique_ptr<ast::integer>>(val)) {
            object_property(out, opt, parent, name, *std::get<std::unique_ptr<ast::integer>>(val));
        } else if (std::holds_alternative<std::unique_ptr<ast::boolean>>(val)) {
            object_property(out, opt, parent, name, *std::get<std::unique_ptr<ast::boolean>>(val));
        } else if (std::holds_alternative<std::unique_ptr<ast::null>>(val)) {
            object_property(opt, opt, parent, name, *std::get<std::unique_ptr<ast::null>>(val));
        }
    }

    template <typename OutStream>
    void object(OutStream& out, const ast::options& opt, const ast::object& obj) {
        for (auto& pair : obj.properties) {
            object_property(out, opt, obj, pair.first, pair.second);
        }
    }

    template <typename OutStream>
    void schema_function(OutStream& out, const ast::options& opt, const ast::message& message) {
        out << "    [[nodiscard]] const rapidjson::SchemaDocument& message_schema() const {\n";
        out << "        static constexpr char schema_data[] = R\"(" << message.schema << ")\";\n";
        out << "        static const auto schema_document = load_schema_document(schema_data);\n";
        out << "        return schema_document;\n";
        out << "    }\n";
    }

    template <typename OutStream>
    void message_class(OutStream& out, const ast::options& opt, const ast::message& message) {
        out << "namespace " << message.cpp_namespace << " {\n";

        if (std::holds_alternative<std::unique_ptr<ast::object>>(message.root)) {
            out << "class " << message.cpp_class
                << " : public fi::patterns::message::support::message, public fi::patterns::message::support::object "
                   "{\n";
            out << "public:\n";
            message_object_constructors(out, opt, message);
            object(out, opt, *std::get<std::unique_ptr<ast::object>>(message.root));
        } else if (std::holds_alternative<std::unique_ptr<ast::array>>(message.root)) {
            auto& array   = *std::get<std::unique_ptr<ast::array>>(message.root);
            auto  subtype = items_type_name(array.items);

            if (subtype == "object") {
                object_class(out, opt, *std::get<std::unique_ptr<ast::object>>(array.items), "root_object");
                subtype = "root_object";
            }
            out << "class " << message.cpp_class
                << " : public fi::patterns::message::support::message, public "
                   "fi::patterns::message::support::array<"
                << subtype << "> {\n";
            out << "public:\n";
            message_array_constructors(out, opt, message, subtype);
        }

        validation_functions(out, opt, message);
        schema_function(out, opt, message);
        out << "};\n";
        out << "}\n";
    }

    template <typename OutStream>
    void object_class(OutStream& out, const ast::options& opt, const ast::object& obj, const std::string& class_name) {
        out << "class " << class_name << " : public fi::patterns::message::support::object {\n";
        out << "public:\n";
        object_constructors(out, opt, obj, class_name);
        object(out, opt, obj);
        out << "};\n";
    }

    template <typename OutStream>
    void message_header(OutStream& out, const ast::options& opt, const ast::message& message) {
        out << "#include <optional>\n";
        out << "#include <rapidjson/document.h>\n";
        out << "#include <rapidjson/schema.h>\n";
        out << "#include <rapidjson/stringbuffer.h>\n";
        out << "#include <rapidjson/writer.h>\n";
        out << "#include <fi/patterns/message/support/accessor.hpp>\n";
        out << "#include <fi/patterns/message/support/array.hpp>\n";
        out << "#include <fi/patterns/message/support/error.hpp>\n";
        out << "#include <fi/patterns/message/support/message.hpp>\n";
        out << "#include <fi/patterns/message/support/object.hpp>\n";
        out << "\n";
        message_class(out, opt, message);
    }

}  // namespace fi::patterns::message::generator
