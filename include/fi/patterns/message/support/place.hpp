#pragma once

#include <rapidjson/document.h>
#include <string>
#include <string_view>

namespace fi::patterns::message::support {

    template <typename T>
    inline void place_val(rapidjson::Value& val, T&& item, rapidjson::Document::AllocatorType& allocator) {
        // Maybe make no-op
        // Require descendent of object.
        //    val.Set(item, allocator);
        //    val.Seto
    }

    template <>
    inline void place_val(rapidjson::Value& val, bool&& item, rapidjson::Document::AllocatorType& allocator) {
        val.SetBool(item);
    }

    template <>
    inline void place_val(rapidjson::Value& val, float&& item, rapidjson::Document::AllocatorType& allocator) {
        val.SetFloat(item);
    }

    template <>
    inline void place_val(rapidjson::Value& val, double&& item, rapidjson::Document::AllocatorType& allocator) {
        val.SetDouble(item);
    }

    template <>
    inline void place_val(rapidjson::Value& val, int32_t&& item, rapidjson::Document::AllocatorType& allocator) {
        val.SetInt(item);
    }

    template <>
    inline void place_val(rapidjson::Value& val, uint32_t&& item, rapidjson::Document::AllocatorType& allocator) {
        val.SetUint(item);
    }

    template <>
    inline void place_val(rapidjson::Value& val, int64_t&& item, rapidjson::Document::AllocatorType& allocator) {
        val.SetInt64(item);
    }

    template <>
    inline void place_val(rapidjson::Value& val, uint64_t&& item, rapidjson::Document::AllocatorType& allocator) {
        val.SetUint64(item);
    }

    template <>
    inline void place_val(rapidjson::Value& val, std::string&& item, rapidjson::Document::AllocatorType& allocator) {
        val.SetString(item.data(), item.size(), allocator);
    }

    template <>
    inline void place_val(rapidjson::Value& val, std::string_view&& item, rapidjson::Document::AllocatorType& allocator) {
        val.SetString(item.data(), item.size(), allocator);
    }

}  // namespace fi::patterns::message::support
