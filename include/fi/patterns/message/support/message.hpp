#pragma once

#include <fi/patterns/message/support/error.hpp>
#include <optional>
#include <rapidjson/document.h>
#include <rapidjson/schema.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

namespace fi::patterns::message::support {

    class message {
    public:
        message() = default;
        explicit message(rapidjson::Type type)
            : _document(type) {}

        template <typename OutputStream>
        void to_json(OutputStream& out) const {
            auto writer = rapidjson::Writer<OutputStream>{out};
            _document.Accept(writer);
        }

        [[nodiscard]] rapidjson::StringBuffer to_json() const {
            auto buffer = rapidjson::StringBuffer{};
            to_json(buffer);
            return buffer;
        }

        [[nodiscard]] const rapidjson::Document& to_document() const { return _document; }

    protected:
        inline std::optional<fi::patterns::message::validation_error>
        validate_impl(const rapidjson::SchemaDocument& message_schema) noexcept {
            if (_document.HasParseError()) return fi::patterns::message::validation_error::build(_document);
            rapidjson::SchemaValidator validator(message_schema);
            if (!_document.Accept(validator))
                return fi::patterns::message::validation_error::build(_document, validator);
            return std::nullopt;
        }

        inline void validate_throw_impl(const rapidjson::SchemaDocument& message_schema) {
            if (_document.HasParseError()) throw fi::patterns::message::validation_error::build(_document);
            rapidjson::SchemaValidator validator(message_schema);
            if (!_document.Accept(validator))
                throw fi::patterns::message::validation_error::build(_document, validator);
        }

        inline rapidjson::SchemaDocument load_schema_document(const char* schema_data) const {
            auto stream   = rapidjson::StringStream(schema_data);
            auto document = rapidjson::Document{};
            document.ParseStream(stream);
            return rapidjson::SchemaDocument{document};
        }

        rapidjson::Document _document;
    };

}  // namespace fi::patterns::message::support
