#pragma once

#include <fi/patterns/message/support/extract.hpp>
#include <fmt/format.h>
#include <rapidjson/document.h>
#include <stdexcept>

namespace fi::patterns::message::support {

    template <typename T>
    class array {
    public:
        array(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator)
            : _value(value)
            , _allocator(allocator) {}

        auto operator[](std::size_t idx) {
            assert(_value.IsArray());
            if (idx >= _value.Size())
                throw std::invalid_argument(fmt::format("Array index {} is out of the bounds {}.", idx, _value.Size()));
            return extract_val<T>(_value[idx], _allocator);
        }

        auto operator[](std::size_t idx) const {
            assert(_value.IsArray());
            if (idx >= _value.Size())
                throw std::invalid_argument(fmt::format("Array index {} is out of the bounds {}.", idx, _value.Size()));
            return extract_val<T>(_value[idx], _allocator);
        }

        auto insert() {
            rapidjson::Value new_value(rapidjson::kObjectType);
            _value.PushBack(new_value, _allocator);
            auto itr = _value.End();
            --itr;
            return T(*itr, _allocator);
        }

        [[nodiscard]] auto size() const { return _value.Size(); }

        [[nodiscard]] bool empty() const { return _value.Empty(); }

    private:
        rapidjson::Value&                   _value;
        rapidjson::Document::AllocatorType& _allocator;
    };

}  // namespace fi::patterns::message::support
