#pragma once

#include <fi/patterns/message/support/array.hpp>
#include <fi/patterns/message/support/extract.hpp>
#include <fi/patterns/message/support/place.hpp>
#include <fmt/format.h>
#include <optional>
#include <rapidjson/document.h>

namespace fi::patterns::message::support {

    template <bool Required, typename T>
    struct required_wrap {};

    template <typename T>
    struct required_wrap<true, T> {
        using type = T;
    };

    template <typename T>
    struct required_wrap<false, T> {
        using type = std::optional<T>;
    };

    template <typename T, bool Required = false>
    struct accessor {
        static inline std::optional<T> get_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                                 const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                return {};
            } else {
                return {T{itr->value, allocator}};
            }
        }
    };

    template <typename T>
    struct accessor<T, true> {
        static inline T get_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                  const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                assert(false);
                throw std::runtime_error("Reached impossible case.");
            } else {
                return T{itr->value, allocator};
            }
        }
    };

    template <typename T>
    struct accessor<array<T>, false> {
        static inline array<T> get_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                         const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                auto new_value = rapidjson::Value(rapidjson::kArrayType);
                value.AddMember(rapidjson::Value().SetString(field_name, allocator), new_value, allocator);
                return array<T>{value.FindMember(field_name)->value, allocator};
            } else {
                return array<T>{itr->value, allocator};
            }
        }
    };

    template <typename T>
    struct accessor<array<T>, true> {
        static inline array<T> get_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                         const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                auto new_value = rapidjson::Value(rapidjson::kArrayType);
                value.AddMember(rapidjson::Value().SetString(field_name, allocator), new_value, allocator);
                return array<T>{value.FindMember(field_name)->value, allocator};
            } else {
                return array<T>{itr->value, allocator};
            }
        }
    };

    template <>
    struct accessor<std::string_view, false> {
        static inline std::optional<std::string_view> get_value(const rapidjson::Value&                   value,
                                                                const rapidjson::Document::AllocatorType& allocator,
                                                                const char*                               field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                return {};
            } else {
                assert(itr->value.IsString());
                return {{itr->value.GetString(), itr->value.GetStringLength()}};
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, const std::optional<std::string_view>& input_val) {
            if (input_val) {
                auto itr = value.FindMember(field_name);
                if (itr != value.MemberEnd()) value.RemoveMember(field_name);
                value.AddMember(
                    rapidjson::Value().SetString(field_name, allocator),
                    rapidjson::Value().SetString(input_val.value().data(), input_val.value().size(), allocator),
                    allocator);
            }
        }
    };

    template <>
    struct accessor<std::string_view, true> {
        static inline std::string_view get_value(const rapidjson::Value&                   value,
                                                 const rapidjson::Document::AllocatorType& allocator,
                                                 const char*                               field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                throw std::runtime_error(fmt::format("Required field '{}' was not found in object.", field_name));
            } else {
                return {itr->value.GetString(), itr->value.GetStringLength()};
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, const std::string_view& input_val) {
            auto itr = value.FindMember(field_name);
            if (itr != value.MemberEnd()) value.RemoveMember(field_name);
            value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                            rapidjson::Value().SetString(input_val.data(), input_val.size(), allocator),
                            allocator);
        }
    };

    template <>
    struct accessor<double, false> {
        static inline std::optional<double> get_value(const rapidjson::Value&                   value,
                                                      const rapidjson::Document::AllocatorType& allocator,
                                                      const char*                               field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                return {};
            } else {
                assert(itr->value.IsDouble());
                return {itr->value.GetDouble()};
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, std::optional<double> input_val) {
            if (input_val) {
                auto itr = value.FindMember(field_name);
                if (itr != value.MemberEnd()) value.RemoveMember(field_name);
                value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                                rapidjson::Value().SetDouble(input_val.value()),
                                allocator);
            } else {
                value.RemoveMember(field_name);
            }
        }
    };

    template <>
    struct accessor<double, true> {
        static inline double get_value(const rapidjson::Value&                   value,
                                       const rapidjson::Document::AllocatorType& allocator, const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                throw std::runtime_error(fmt::format("Required field '{}' was not found in object.", field_name));
            } else {
                assert(itr->value.IsDouble());
                return itr->value.GetDouble();
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, double input_val) {
            auto itr = value.FindMember(field_name);
            if (itr != value.MemberEnd()) value.RemoveMember(field_name);
            value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                            rapidjson::Value().SetDouble(input_val),
                            allocator);
        }
    };

    template <>
    struct accessor<float, false> {
        static inline std::optional<float> get_value(const rapidjson::Value&                   value,
                                                     const rapidjson::Document::AllocatorType& allocator,
                                                     const char*                               field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                return {};
            } else {
                assert(itr->value.IsFloat());
                return {itr->value.GetFloat()};
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, std::optional<float> input_val) {
            if (input_val) {
                auto itr = value.FindMember(field_name);
                if (itr != value.MemberEnd()) value.RemoveMember(field_name);
                value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                                rapidjson::Value().SetFloat(input_val.value()),
                                allocator);
            } else {
                value.RemoveMember(field_name);
            }
        }
    };

    template <>
    struct accessor<float, true> {
        static inline float get_value(const rapidjson::Value&                   value,
                                      const rapidjson::Document::AllocatorType& allocator, const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                throw std::runtime_error(fmt::format("Required field '{}' was not found in object.", field_name));
            } else {
                assert(itr->value.IsFloat());
                return itr->value.GetFloat();
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, float input_val) {
            auto itr = value.FindMember(field_name);
            if (itr != value.MemberEnd()) value.RemoveMember(field_name);
            value.AddMember(
                rapidjson::Value().SetString(field_name, allocator), rapidjson::Value().SetFloat(input_val), allocator);
        }
    };

    template <>
    struct accessor<uint32_t, false> {
        static inline std::optional<uint32_t> get_value(const rapidjson::Value&                   value,
                                                        const rapidjson::Document::AllocatorType& allocator,
                                                        const char*                               field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                return {};
            } else {
                assert(itr->value.IsUint());
                return {itr->value.GetUint()};
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, std::optional<uint32_t> input_val) {
            if (input_val) {
                auto itr = value.FindMember(field_name);
                if (itr != value.MemberEnd()) value.RemoveMember(field_name);
                value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                                rapidjson::Value().SetUint(input_val.value()),
                                allocator);
            } else {
                value.RemoveMember(field_name);
            }
        }
    };

    template <>
    struct accessor<uint32_t, true> {
        static inline uint32_t get_value(const rapidjson::Value&                   value,
                                         const rapidjson::Document::AllocatorType& allocator, const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                throw std::runtime_error(fmt::format("Required field '{}' was not found in object.", field_name));
            } else {
                assert(itr->value.IsUint());
                return itr->value.GetUint();
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, uint32_t input_val) {
            auto itr = value.FindMember(field_name);
            if (itr != value.MemberEnd()) value.RemoveMember(field_name);
            value.AddMember(
                rapidjson::Value().SetString(field_name, allocator), rapidjson::Value().SetUint(input_val), allocator);
        }
    };

    template <>
    struct accessor<int32_t, false> {
        static inline std::optional<int32_t> get_value(const rapidjson::Value&                   value,
                                                       const rapidjson::Document::AllocatorType& allocator,
                                                       const char*                               field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                return {};
            } else {
                assert(itr->value.IsInt());
                return {itr->value.GetInt()};
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, std::optional<int32_t> input_val) {
            if (input_val) {
                auto itr = value.FindMember(field_name);
                if (itr != value.MemberEnd()) value.RemoveMember(field_name);
                value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                                rapidjson::Value().SetInt(input_val.value()),
                                allocator);
            } else {
                value.RemoveMember(field_name);
            }
        }
    };

    template <>
    struct accessor<int32_t, true> {
        static inline int32_t get_value(const rapidjson::Value&                   value,
                                        const rapidjson::Document::AllocatorType& allocator, const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                throw std::runtime_error(fmt::format("Required field '{}' was not found in object.", field_name));
            } else {
                assert(itr->value.IsInt());
                return itr->value.GetInt();
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, int32_t input_val) {
            auto itr = value.FindMember(field_name);
            if (itr != value.MemberEnd()) value.RemoveMember(field_name);
            value.AddMember(
                rapidjson::Value().SetString(field_name, allocator), rapidjson::Value().SetInt(input_val), allocator);
        }
    };

    template <>
    struct accessor<uint64_t, false> {
        static inline std::optional<uint64_t> get_value(const rapidjson::Value&                   value,
                                                        const rapidjson::Document::AllocatorType& allocator,
                                                        const char*                               field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                return {};
            } else {
                assert(itr->value.IsUint64());
                return {itr->value.GetUint64()};
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, std::optional<uint64_t> input_val) {
            if (input_val) {
                auto itr = value.FindMember(field_name);
                if (itr != value.MemberEnd()) value.RemoveMember(field_name);
                value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                                rapidjson::Value().SetUint64(input_val.value()),
                                allocator);
            } else {
                value.RemoveMember(field_name);
            }
        }
    };

    template <>
    struct accessor<uint64_t, true> {
        static inline uint64_t get_value(const rapidjson::Value&                   value,
                                         const rapidjson::Document::AllocatorType& allocator, const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                throw std::runtime_error(fmt::format("Required field '{}' was not found in object.", field_name));
            } else {
                assert(itr->value.IsUint64());
                return itr->value.GetUint64();
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, uint64_t input_val) {
            auto itr = value.FindMember(field_name);
            if (itr != value.MemberEnd()) value.RemoveMember(field_name);
            value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                            rapidjson::Value().SetUint64(input_val),
                            allocator);
        }
    };

    template <>
    struct accessor<int64_t, false> {
        static inline std::optional<int64_t> get_value(const rapidjson::Value&                   value,
                                                       const rapidjson::Document::AllocatorType& allocator,
                                                       const char*                               field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                return {};
            } else {
                assert(itr->value.IsInt64());
                return {itr->value.GetInt64()};
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, std::optional<int64_t> input_val) {
            if (input_val) {
                auto itr = value.FindMember(field_name);
                if (itr != value.MemberEnd()) value.RemoveMember(field_name);
                value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                                rapidjson::Value().SetInt64(input_val.value()),
                                allocator);
            } else {
                value.RemoveMember(field_name);
            }
        }
    };

    template <>
    struct accessor<int64_t, true> {
        static inline int64_t get_value(const rapidjson::Value&                   value,
                                        const rapidjson::Document::AllocatorType& allocator, const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                throw std::runtime_error(fmt::format("Required field '{}' was not found in object.", field_name));
            } else {
                assert(itr->value.IsInt64());
                return itr->value.GetInt64();
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, int64_t input_val) {
            auto itr = value.FindMember(field_name);
            if (itr != value.MemberEnd()) value.RemoveMember(field_name);
            value.AddMember(
                rapidjson::Value().SetString(field_name, allocator), rapidjson::Value().SetInt64(input_val), allocator);
        }
    };

    template <>
    struct accessor<bool, false> {
        static inline std::optional<bool> get_value(const rapidjson::Value&                   value,
                                                    const rapidjson::Document::AllocatorType& allocator,
                                                    const char*                               field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                return {};
            } else {
                assert(itr->value.IsBool());
                return {itr->value.GetBool()};
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, std::optional<bool> input_val) {
            if (input_val) {
                auto itr = value.FindMember(field_name);
                if (itr != value.MemberEnd()) value.RemoveMember(field_name);
                value.AddMember(rapidjson::Value().SetString(field_name, allocator),
                                rapidjson::Value().SetBool(input_val.value()),
                                allocator);
            } else {
                value.RemoveMember(field_name);
            }
        }
    };

    template <>
    struct accessor<bool, true> {
        static inline bool get_value(const rapidjson::Value& value, const rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name) {
            assert(value.IsObject());
            auto itr = value.FindMember(field_name);
            if (itr == value.MemberEnd()) {
                throw std::runtime_error(fmt::format("Required field '{}' was not found in object.", field_name));
            } else {
                assert(itr->value.IsBool());
                return itr->value.GetBool();
            }
        }

        static inline void set_value(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator,
                                     const char* field_name, bool input_val) {
            auto itr = value.FindMember(field_name);
            if (itr != value.MemberEnd()) value.RemoveMember(field_name);
            value.AddMember(
                rapidjson::Value().SetString(field_name, allocator), rapidjson::Value().SetBool(input_val), allocator);
        }
    };

}  // namespace fi::patterns::message::support
