#pragma once

#include <rapidjson/document.h>
#include <string>
#include <string_view>

namespace fi::patterns::message::support {

    template <typename T>
    inline auto extract_val(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        return T(val, allocator);
    }

    template <>
    inline auto extract_val<bool>(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        assert(val.IsBool());
        return val.GetBool();
    }

    template <>
    inline auto extract_val<float>(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        assert(val.IsFloat());
        return val.GetFloat();
    }

    template <>
    inline auto extract_val<double>(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        assert(val.IsDouble());
        return val.GetDouble();
    }

    template <>
    inline auto extract_val<int32_t>(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        assert(val.IsInt());
        return val.GetInt();
    }

    template <>
    inline auto extract_val<uint32_t>(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        assert(val.IsUint());
        return val.GetUint();
    }

    template <>
    inline auto extract_val<int64_t>(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        assert(val.IsInt64());
        return val.GetInt64();
    }

    template <>
    inline auto extract_val<uint64_t>(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        assert(val.IsUint64());
        return val.GetUint64();
    }

    template <>
    inline auto extract_val<std::string>(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        assert(val.IsString());
        return std::string_view(val.GetString(), val.GetStringLength());
    }

    template <>
    inline auto extract_val<std::string_view>(rapidjson::Value& val, rapidjson::Document::AllocatorType& allocator) {
        assert(val.IsString());
        return std::string_view(val.GetString(), val.GetStringLength());
    }

}  // namespace fi::patterns::message::support
