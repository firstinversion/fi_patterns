#pragma once

#include <exception>
#include <fi/patterns/export.hpp>
#include <rapidjson/fwd.h>
#include <string>

namespace fi::patterns::message {

    class FI_PATTERNS_EXPORT validation_error : public std::exception {
    public:
        FI_PATTERNS_EXPORT static validation_error build(const rapidjson::Document& doc);
        FI_PATTERNS_EXPORT static validation_error build(const rapidjson::Document&        doc,
                                                         const rapidjson::SchemaValidator& validator);

        explicit validation_error(std::string what)
            : _what(std::move(what)) {}

        [[nodiscard]] const char* what() const noexcept override { return _what.data(); }

    private:
        std::string _what;
    };

}  // namespace fi::patterns::message
