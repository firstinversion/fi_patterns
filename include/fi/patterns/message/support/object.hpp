#pragma once

#include <rapidjson/document.h>

namespace fi::patterns::message::support {

    class object {
    public:
        object(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator)
            : _value(value)
            , _allocator(allocator) {}

    protected:
        rapidjson::Value&                   _value;
        rapidjson::Document::AllocatorType& _allocator;
    };

}  // namespace fi::patterns::message::support
