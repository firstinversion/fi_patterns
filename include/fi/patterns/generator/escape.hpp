#pragma once

#include <regex>
#include <string>

namespace fi::patterns::generator {

    template <typename OutStream, typename BidirItr>
    void escape(OutStream& out, BidirItr begin, BidirItr end) {
        static const auto regex = std::regex{"\""};
        std::regex_replace(std::ostreambuf_iterator<char>(out), begin, end, regex, "\\\"");
    }

}  // namespace fi::patterns::generator
