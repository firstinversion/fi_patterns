#pragma once

#include <fi/patterns/message/ast.hpp>
#include <fi/patterns/message/generator.hpp>
#include <fi/patterns/message/parser.hpp>
#include <fi/patterns/message/translate.hpp>
