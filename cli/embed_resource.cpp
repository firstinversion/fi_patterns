#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>

int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr,
                "USAGE: %s {sym} {rsrc}\n\n"
                "  Creates {sym}.c from the contents of {rsrc}\n",
                argv[0]);
        return EXIT_FAILURE;
    }

    std::filesystem::path dst_cpp{argv[1]};
    std::filesystem::path dst_hpp{argv[2]};
    std::filesystem::path src{argv[3]};

    std::string sym = src.filename().string();
    std::replace(sym.begin(), sym.end(), '.', '_');
    std::replace(sym.begin(), sym.end(), '-', '_');

    create_directories(dst_cpp.parent_path());

    std::ofstream ofs_cpp{dst_cpp};
    std::ofstream ofs_hpp{dst_hpp};
    std::ifstream ifs{src};

    // Generate Cpp
    ofs_cpp << "#include <cstdlib>" << '\n';
    ofs_cpp << "namespace embed::resource {\n";
    ofs_cpp << "extern const char " << sym << "[] = {\n";

    size_t lineCount = 0;
    while (!ifs.eof()) {
        char c;
        ifs.get(c);
        ofs_cpp << "0x" << std::hex << (c & 0xff) << ", ";  // NOLINT(hicpp-signed-bitwise)
        if (++lineCount == 10) {
            ofs_cpp << '\n';
            lineCount = 0;
        }
    }

    ofs_cpp << "};" << '\n';
    ofs_cpp << "extern const size_t " << sym << "_len = sizeof(" << sym << ");\n";
    ofs_cpp << "}\n";

    // Generate Hpp
    ofs_hpp << "#pragma once\n";
    ofs_hpp << "#include <cstdlib>\n";
    ofs_hpp << "namespace embed::resource {\n";
    ofs_hpp << "extern const char " << sym << "[];\n";
    ofs_hpp << "extern const size_t " << sym << "_len;\n";
    ofs_hpp << "}\n";

    return EXIT_SUCCESS;
}
