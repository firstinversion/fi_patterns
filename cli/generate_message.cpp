#include <boost/program_options.hpp>
#include <fi/patterns/message/translate.hpp>
#include <filesystem>
#include <fmt/format.h>
#include <iostream>

namespace po = boost::program_options;

int main(int argc, char** argv) {
    auto description = po::options_description{"Generator Options"};
    // clang-format off
    description.add_options()
        ("input,i", po::value<std::string>(), "File path of message file to use.")
        ("output,o", po::value<std::string>(), "File path of where to place output header file.")
        ("help,h", "Show help message.")
        ;
    // clang-format on

    auto vm = po::variables_map{};
    po::store(po::parse_command_line(argc, argv, description), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << description << '\n';
        return 1;
    }

    if (vm.count("input") == 0) {
        fmt::print("Input file not specified, use '--input' or '-i'.\n");
        return 1;
    }

    if (vm.count("output") == 0) {
        fmt::print("Output file not specified, use '--output' or '-o'.\n");
        return 1;
    }

    auto input_file  = std::filesystem::path{vm["input"].as<std::string>()};
    auto output_file = std::filesystem::path{vm["output"].as<std::string>()};

    try {
        fi::patterns::message::translate::message_header({}, input_file, output_file);
        return 0;
    } catch (const std::exception& err) {
        fmt::print("{}\n", err.what());
        return 1;
    }
}
